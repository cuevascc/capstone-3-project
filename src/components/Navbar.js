import React, {useContext, useState} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import {Container, NavDropdown} from 'react-bootstrap'
import {NavLink, Link, Redirect} from 'react-router-dom'

import UserContext from '../userContext'
import cart from '../pages/images/cart.png'

export default function NavBar() {

	const {user, unsetUser, setUser} = useContext(UserContext)

	const [willRedirect, setWillRedirect] = useState(false)

	function logout() {
	    unsetUser()
	    setUser({
	        email: null,
	        isAdmin: null
	    })

	    setWillRedirect(true)
	}

	return (

		<>

		{willRedirect && (<Redirect to="/signin"/>)}

		<Navbar className="navbar" expand="sm" sticky="top">
		  <Container className="navbar">
		    <Navbar.Brand id="brand" as={Link} to="/">CJEWELS</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse className="navbar" id="basic-navbar-nav">
		      <Nav className="ml-auto">
		    		{
		    			user.isAdmin

		    			?
		    				<>
		    					<Nav.Link className="navbar" as={NavLink} to="/products">Products</Nav.Link>
		    					<Nav.Link className="navbar" as={NavLink} to="/addproduct">Add Product</Nav.Link>
		    					<Nav.Link className="navbar" as={NavLink} to="/orders">Orders</Nav.Link>
		    				</>
		    			:
		    				<>
		    					<Nav.Link className="navbar" as={NavLink} to="/products">Products</Nav.Link>
		    					<Nav.Link className="navbar" as={NavLink} to="/cart"><img src={cart} alt="cart" />Cart</Nav.Link>
		    				</>
		    		}
		      </Nav>
		      <NavDropdown title="Account" id="nav-dropdown"className="navbar mx-0">
		      		{
		      			user.isAdmin
		      			?
		      					<NavDropdown.Item className="nav-dropdown-item" onClick={logout}>Logout</NavDropdown.Item>
		      			:
		      				<>
		      					{
		      						user.email

		      						?
		      							<>
		      								<NavDropdown.Item className="nav-dropdown-item" as={NavLink} to="/orders">Orders</NavDropdown.Item>
		      								<NavDropdown.Divider />
		      								<NavDropdown.Item className="nav-dropdown-item" onClick={logout}>Logout</NavDropdown.Item>
		      							</>
		      						:
		      							<>
		      								<NavDropdown.Item className="nav-dropdown-item" as={NavLink} to="/signup">Sign Up</NavDropdown.Item>
		      								<NavDropdown.Item className="nav-dropdown-item" as={NavLink} to="/signin">Sign in</NavDropdown.Item>
		      							</>
		      					}
		      				</>
		      		}
		      </NavDropdown>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

		</>


		)
}