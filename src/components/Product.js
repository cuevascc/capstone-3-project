import React, {useState, useContext} from 'react'
import {Card} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'

import nextArrow from '../pages/images/next-arrow.png'
import UserContext from '../userContext'

export default function Product({productProp}) {

	const {user} = useContext(UserContext)

	const [willRedirect, setWillRedirect] = useState(false)
	const [storedData, setStoredData] = useState({})

	function viewProduct (productId) {
		fetch(`https://quiet-badlands-82282.herokuapp.com/api/products/${productId}`)

		.then(res => res.json())
		.then(data => {

			data = data.searchResult

			setStoredData(data)

			setWillRedirect(true)	
		})
	}

	return (
			<>
				{willRedirect && (<Redirect to={{
					pathname: "/productdetails",
					state: storedData
				}}/>)}
					
						<div className="col-lg-3" id="productColumn">
								<Card className="productCard">
								<Card.Img variant="top" src={productProp.productImage} />
								  <Card.Body>
								    <Card.Title>{productProp.productName}</Card.Title>
								  </Card.Body>
								  <div className="productPrice">
								  	<div className="row">	
								  		<div className="col details align-middle">
								  			<p className="product-price align-middle">Price: ₱ {productProp.price.toFixed(2)}</p>
								  		</div>
								  		<div className="col-lg-3 details">
								  			<div className="next-arrow">
								  				{
								  					user.email
								  					?
								  						<a onClick={() =>viewProduct(productProp._id)}><img src={nextArrow} alt="go to product" align="right"/></a>
								  					:
								  						<a href="/signin"><img src={nextArrow} align="right"/></a>
								  				}	
								  				
								  			</div>
								  		</div>
								  	</div>
								  </div>			
								</Card>
						</div>
			</>

		)
}