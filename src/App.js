import React, {useState} from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap'
import './App.css'

import {UserProvider} from './userContext'

import NavBar from './components/Navbar'
import Home from './pages/home'
import SignUp from './pages/signup'
import SignIn from './pages/signin'
import Products from './pages/products'
import ViewProductDetails from './pages/productDetails'
import EditProductDetails from './pages/editProductDetails'
import AddProduct from './pages/addProducts'
import Cart from './pages/cart'
import CheckOut from './pages/checkOut'
import Orders from './pages/orders'
import UserProfile from './pages/userProfile'


function App() {

    const [cart, setCart] = useState([])

    const [user, setUser] = useState({
            email: localStorage.getItem('email'),
            isAdmin: localStorage.getItem('isAdmin') === "true"
    })

    function unsetUser() {
        localStorage.clear()
    }


    function addProductToCart (product) {
        let index = cart.findIndex(item => item.productId == product.productId)
        if(index == -1) {
            cart.push(product)
        } else {
            cart[index].quantity += product.quantity
        }
         setCart(cart)
    }

    function emptyCart(){
        setCart([])
    }


  return (
    <>  
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
                <NavBar />
                <Container>
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/signup" component={SignUp} />
                        <Route exact path="/signin" component={SignIn} />
                        <Route exact path="/orders" component={Orders} />
                        <Route exact path="/userprofile" component={UserProfile} />
                        <Route exact path='/productdetails' render={() => <ViewProductDetails addProductToCart={addProductToCart} />}
                        />
                        <Route exact path='/cart' render={() => <Cart cart={cart} emptyCart = {emptyCart}/>}
                        />
                        <Route exact path="/checkout" component={CheckOut} />
                        <Route exact path="/editproductdetails" component={EditProductDetails} />
                        <Route exact path="/addproduct" component={AddProduct} />
                        <div>   
                            <div className="row">   
                                <Route  className="row" exact path="/products" component={Products} />
                            </div>
                        </div>
                        
                               
                    </Switch>
                </Container>
            </Router>
        </UserProvider>
       
    </>
    
  );

}

export default App;
