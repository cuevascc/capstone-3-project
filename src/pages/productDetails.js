import React, {useState, useContext} from 'react'
import {useLocation, Redirect} from "react-router-dom";
import {Card, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'

import cart from './images/cart.png'
import imageholder from '../pages/images/Carousel-3.jpg'

export default function ViewProductDetails ({addProductToCart}) {

	let location = useLocation();

	const [stocks, setStocks] = useState(location.state.stocks)
	const [willRedirect, setWillRedirect] = useState()
	const [quantity, setQuantity] = useState(0)

	let productId = location.state._id
	let productName = location.state.productName
	let productDescription = location.state.productDescription
	let productImage = location.state.productImage
	let price = location.state.price
	let weight = location.state.weight


	function addToCart () {
			addProductToCart({
				productId: productId,
				productName: productName,
				productDescription: productDescription,
				productImage: productImage,
				stocks: stocks,
				weight: weight,
				price: price,
				quantity: quantity
			})

			Swal.fire({
				icon: "success",
				confirmButtonColor: '#eb967a',
				title: "Added to Cart successfully!"
			})

			setWillRedirect(true)		
	}

	function increment () {
		setQuantity(quantity + 1)
		setStocks(stocks - 1)
	}

	function decrement () {
		setQuantity(quantity - 1)
		setStocks(stocks + 1)
	}

	return (
		willRedirect

			?
				<Redirect to='/cart' />
			:

		<div>
			<div className="row justify-content-center">
				<div id="viewProduct" className="col-lg-6">
					<Card className="single-product-card">	
						<Card.Img variant="top" src={productImage} />
						<Card.Body>
							<Card.Title>{productName}</Card.Title>
							<Card.Text className="product-description">
								{productDescription}
							</Card.Text>
						</Card.Body>
						<div className="productPrice">
							<div className="row">	
								<div className="col product-price-stocks">
									<p>Price: ₱ {price.toFixed(2)}</p>
									<p>Stocks: {stocks} pcs.</p>
								</div>
							</div>
							<div className="row" id="quantity-box"> 
								<div className="col">
									<Button 
										disabled={quantity==0} 
										onClick={decrement} 
										className="quantity-btn">-
									</Button>
									<span className="quantity-num" type="number ">{quantity}</span>
									<Button 
										disabled={stocks == 0} 
										onClick={increment}
										className="quantity-btn">+
									</Button>
								</div>
								<div className="col-auto" id="cartbtn">
									<Button 
										disabled={quantity==0} 
										onClick={() => addToCart()}
										className="add-to-cart-btn"><img src={cart} />  Add to Cart
									</Button>
								</div>
							</div>
						</div>
					</Card>
				</div>
			</div>
		</div>	
	)
}

