import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'

import UserContext from '../userContext'

export default function SignIn () {

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState ("")
	const [password, setPassword] = useState ("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false) 

	useEffect(() => {
		if((email !== "" && password !== "")) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email, password]);


	function signInUser(e) {
		e.preventDefault()

		fetch('https://quiet-badlands-82282.herokuapp.com/api/login', {

					method: "POST",
					headers: {
						"Content-type": "application/json"
					},
					body: JSON.stringify({
						email: email,
						password: password

					})

				})
				.then(response => response.json())
				.then(data => {
					if(data.message){
						
						Swal.fire({
							icon: "error",
							title: "Login Failed!",
							text: data.message
						})
					} else {

						localStorage.setItem('token', data.accessToken)
						fetch('https://quiet-badlands-82282.herokuapp.com/api/user/profile', {

							headers: {
								Authorization: `Bearer ${data.accessToken}`
							}

						})
						.then(res => res.json())
						.then(data => {

							data = data.userDetails

							localStorage.setItem('email', data.email)
							localStorage.setItem('isAdmin', data.isAdmin)
							localStorage.setItem('firstName', data.firstName)

							setUser({
								email: data.email,
								firstName: data.firstName,
								isAdmin: data.isAdmin
							})

							setWillRedirect(true)
							
							Swal.fire({
								icon: "success",
								title: "Login Successful!",
								confirmButtonColor: '#eb967a',
								text: `Thank you for logging in, ${data.firstName}!`
							})

						})
					}
				})

				setEmail("")
				setPassword("")
	}

	return (

			user.email || willRedirect

			?
				<Redirect to='/' />

			:
				<div id="sign-section">
					<div className="row justify-content-center">
						<div id="signin-box" className="col-lg-4">
							<div className="sign-header">
								<h2>SIGN IN</h2>
							</div>
							<Form onSubmit={e=> signInUser(e)} className="form-box">
								<Form.Group className="form-item">
									<Form.Label>EMAIL</Form.Label>
									<Form.Control 
										type="email" 
										value={email} 
										onChange={e=>{setEmail(e.target.value)}} 
										required/>
								</Form.Group>
								<Form.Group className="form-item">
									<Form.Label>PASSWORD</Form.Label>
									<Form.Control 
										type="password" 
										value={password} 
										onChange={e=>{setPassword(e.target.value)}} 
										required/>
								</Form.Group>
								<div className="sign-button">
									<Button 
										variant="custom" 
										type="submit" 
										disabled={isActive==false}>
										Sign In
									</Button>
								</div>
							</Form>
							<div className="signin-to-reg">
									Not yet a member? <a href="/signup">Sign Up Here</a> 
							</div>
						</div>
					</div>
				</div>
		)
}

	

