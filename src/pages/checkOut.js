import React, {useState, useEffect, useContext} from 'react'
import {useLocation, Redirect} from "react-router-dom";
import {Card, Button, Form, Table} from 'react-bootstrap'
import Swal from 'sweetalert2'

import UserContext from '../userContext'

export default function CheckOut () {

	const {user} = useContext(UserContext)

	const [shippingAddress, setShippingAddress] = useState("")
	const [status, setStatus] = useState(false)
	const [region, setRegion] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(() => {

		if(shippingAddress !== "" && region !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[shippingAddress, region])

	let location = useLocation();

	let products = location.state
	let subTotal = 0
	let totalWeight = 0
	let shippingFee = 0
	let total = 0

	products.forEach(item => {
		let eachTotal = (item.price*item.quantity)
		subTotal += eachTotal
		totalWeight += item.weight

	})

	let productList = products.map(product => {
		return (
			<tr key={product.productId}>
				<div>
					<div className="row">
						<div className="col">
							<h6>{product.productName}</h6>
						</div>
					</div>
					<div className="row">
						<div className="col">
							Price: ₱ {product.price.toFixed(2)}
						</div>	
					</div>
					<div className="row">
						<div className="col">
							Quantity: {product.quantity} pcs.
						</div>	
					</div>
				</div>				
			</tr>		
		)
	})

	let orderedProducts = products.map(product => {
		return {
			productId: product.productId,
			quantity: product.quantity,
		}
	})

	let standardShippingFee = 125
	shippingFee = standardShippingFee + (totalWeight*0.05)
	total = subTotal + shippingFee

	function placeOrder () {
		fetch('https://quiet-badlands-82282.herokuapp.com/api/order', {
			method: "POST",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				products: orderedProducts,
				region: region,
				shippingAddress: shippingAddress,
				shippingFee: shippingFee,
				status: status,
				total: total

			})
		})
		.then(res => res.json())
		.then(data => {


			Swal.fire({
				icon: "success",
				title: "Order Placed!",
				confirmButtonColor: '#eb967a',
				text: `Thank you for shopping with us, ${user.firstName}!`
			})

			location.emptyCart()
			setWillRedirect(true)
			setStatus(false)

		})
		setShippingAddress("")
		setRegion("")

	}

	return (
		willRedirect

		?

		<Redirect to='/userprofile' />

		:

		<>
		<div>
			<div className="row justify-content-center">
				<div className="col-lg-5">
					<Card className="cart">
						<div>
							<div className="row justify-content-center">
								<div className="col-lg-10 checkout-page-heading">
									C H E C K O U T
								</div>
							</div>
						</div>
						<Table className="cart-rows" striped hover>
							<tbody>
								{productList} 	
							</tbody>
						</Table>
						<Form>
							<Form.Label className="payment-left">Shipping Address:</Form.Label>
							<Form.Select 
								onChange={e=>setRegion(e.target.value)} 
								className="check-out-form address-form" 
								aria-label="Default select example"
								>
							  <option>Select Region</option>
							  <option value="NCR">National Capital Region</option>
							  <option value="CAR">Cordillera Administrative Region</option>
							  <option value="Ilocos Region">Ilocos Region</option>
							  <option value="Cagayan Valley">Cagayan Valley</option>
							  <option value="Central Luzon">Central Luzon</option>
							  <option value="Calabarzon">Calabarzon</option>
							  <option value="MIMAROPA">MIMAROPA</option>
							  <option value="Bicol Region">Bicol Region</option>
							  <option value="Western Visayas">Western Visayas</option>
							  <option value="Central Visayas">Central Visayas</option>
							  <option value="Eastern Visayas">Eastern Visayas</option>
							  <option value="Zamboanga Peninsula">Zamboanga Peninsula</option>
							  <option value="Northern Mindanao">Northern Mindanao</option>
							  <option value="Davao Region">Davao Region</option>
							  <option value="Soccskargen">Soccskargen</option>
							  <option value="Caraga Region">Caraga Region</option>
							  <option value="BARMM">BARMM</option>
							</Form.Select>
							<Form.Group className="check-out-form">
								<Form.Control
									className="address-form" 
									type="text" 
									value={shippingAddress} 
									placeholder="Unit no., House no., St. Name, Barangay, City"
									onChange={e=>{setShippingAddress(e.target.value)}} required/>
							</Form.Group>
						</Form>
						<div className="payment-details">
							<div className="row">
								<div className="col payment-left">
									Subtotal: 
								</div>
								<div className="col payment-right">
								₱ {subTotal.toFixed(2)}
								</div>
							</div>
							<div className="row">
								<div className="col payment-left">
									Shipping Fee: 
								</div>

								{
									region

									?
										<div className="col payment-right">
										₱ {shippingFee.toFixed(2)}
										</div>
									:

										<div className="col payment-right">
										0.00
										</div>

								}

							</div>
							<div className="row">
								<div className="col payment-left">
									Total Payment: 
								</div>

								{
									region
									?
										<div className="col payment-right">
											 <strong>₱ {total.toFixed(2)}</strong>
										</div>	
									:
										<div className="col payment-right">
											 <strong>₱ {subTotal.toFixed(2)}</strong>
										</div>
								}
									
							</div>
						</div>
						<Button 
							className="check-out-btn" 
							disabled={isActive === false} 
							onClick={placeOrder}>Place order</Button>					 
					</Card>
				</div>
			</div>
		</div>

		</>
	
	)


}