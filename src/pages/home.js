import React from 'react'
import Carousel from 'react-bootstrap/Carousel'
import image1 from './images/Carousel-1.jpg'
import image2 from './images/Carousel-2.jpg'
import image3 from './images/Carousel-3.jpg'



export default function Home () {



	return (

		<Carousel className="carousel" variant="none">
		  <Carousel.Item className="carousel-item">
		    <img
		      className="d-block w-100"
		      src={image1}
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h5>Add elegance to your appearance</h5>
		      <p>We design masterpieces for you</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item className="carousel-item">
		    <img
		      className="d-block w-100"
		      src={image2}
		      alt="Second slide"
		    />
		    <Carousel.Caption>
		      <h5>Elegant jewelry for elegant moments</h5>
		      <p>See our latest collection</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item className="carousel-item">
		    <img
		      className="d-block w-100"
		      src={image3}
		      alt="Third slide"
		    />
		    <Carousel.Caption>
		      <h5>Be the queen of jewels</h5>
		      <p>Look as beautiful outside as you are within</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>

		)
}