import React, {useContext, useEffect, useState} from 'react'
import {Redirect} from 'react-router-dom'
import {Button, Table, Modal} from 'react-bootstrap'

import UserContext from '../userContext'

export default function Orders () {

	const {user} = useContext(UserContext)

	const [order, setOrder] = useState({})
	const [modalShow, setModalShow] = useState(false)
	const [allOrders, setAllOrders] = useState([])
	const [update, setUpdate] = useState(0)

	useEffect(()=> {
		fetch('https://quiet-badlands-82282.herokuapp.com/api/orders', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}	
		})
		.then(res => res.json())
		.then(data => {
			if(!data.orders)
				return

			setAllOrders(data.orders)
		})

	}, [update])


	function fulfillOrder (orderId) {
		fetch(`https://quiet-badlands-82282.herokuapp.com/api/orders/fulfill/${orderId}`, {
					method: 'PUT',
					headers: {
						'Authorization':`Bearer ${localStorage.getItem('token')}`
					}
		})
		.then(res => res.json())
		.then(data => {
			setUpdate({})	
		})
	}

	function viewOrder (orderId) {
		fetch(`https://quiet-badlands-82282.herokuapp.com/api/order/${orderId}`, {
					method: 'GET',
					headers: {
						'Authorization':`Bearer ${localStorage.getItem('token')}`
					}
		})
		.then(res => res.json())
		.then(data => {
			setOrder(data)
			setModalShow(true)
		})
	}

	function MyVerticallyCenteredModal(props) {

		let orderedProducts = order.quantityRef && order.quantityRef.map(product => {
			
			return (
				<>
				<div>
					<div className="row">
						<div className="col modal-view">
							Product Reference ID:
						</div>
						<div className="col modal-view-right">
							{product.productId}
						</div>
					</div>
					<div className="row">
						<div className="col modal-view">
							Quantity:
						</div>
						<div className="col modal-view-right">
							{product.quantity} pcs.
						</div>
					</div>
				</div>
				<hr />
				</>
			)
		})

	  return (
	    <Modal
	      {...props}
	      size="m"
	      aria-labelledby="contained-modal-title-vcenter"
	      centered
	    >
	      <Modal.Header closeButton>
	        <Modal.Title className="modal-view" id="contained-modal-title-vcenter">
	          Reference ID: {order._id}
	        </Modal.Title>
	      </Modal.Header>
	      <Modal.Body>
	        {orderedProducts}
	        <div>
	        	<div className="row">
	        		<div className="col modal-view-left">
	        			Subtotal:
	        		</div>
	        		<div className="col modal-view-right">
	        			₱ {order.subTotal && order.subTotal.toFixed(2)}
	        		</div>
	        	</div>
	        	<div className="row">
	        		<div className="col modal-view-left">
	        			Shipping Fee:
	        		</div>
	        		<div className="col modal-view-right">
	        			₱ {order.shippingFee && order.shippingFee.toFixed(2)}
	        		</div>
	        	</div>
	        	<div className="row">
	        		<div className="col modal-view-left">
	        			<strong>Total Payment:</strong>
	        		</div>
	        		<div className="col modal-view-right">
	        			₱ {order.total && order.total.toFixed(2)}
	        		</div>
	        	</div>
	        </div>
	      </Modal.Body>
	    </Modal>
	  );
	}
		
	let ordersList = allOrders && allOrders.map(order => {
		return (

			<tr key={order._id}>
				<td>{order._id}</td>
				<td>{order.user.email}</td>
				<td>{order.region}</td>
				<td>{order.shippingAddress}</td>
				<td>{order.purchasedOn}</td>
				<td className={order.status ? "text-success" : "text-muted"}>{order.status ? "Fulfilled" : "Pending"}</td>
				<td>
					{
						!order.status && 
	
						<Button 
							className="activate-btn" 
							onClick={()=> fulfillOrder(order._id)}>Fulfill
						</Button>
						
					}
						<Button 
							className="edit-btn" 
							onClick={() => { viewOrder(order._id); }}>View
						</Button>
				</td>
			</tr>
		)
	})

	return (

		user.isAdmin

		?

		<div className="table-container">
			<Table className="products-list" striped bordered hover>
				<thead>
					<tr>
						<th>Order ID</th>
						<th>User</th>
						<th>Shipping Region</th>
						<th>Shipping Address</th>
						<th>Purchase Date</th>
						<th>Status</th>							
						<th>Actions</th>
					</tr>
				</thead>
						<tbody>
							{ordersList}
						</tbody>
			</Table>

			<MyVerticallyCenteredModal
			        show={modalShow}
			        onHide={() => setModalShow(false)}
			 />
		</div>

		:

		<Redirect to="/userprofile" />
	)
}