import React, {useState,useEffect,useContext} from 'react'
import {Redirect} from 'react-router-dom'
import {Form, Button, InputGroup} from 'react-bootstrap'
import Swal from 'sweetalert2'

import UserContext from '../userContext'

export default function AddProduct() {

	const {user} = useContext(UserContext)

	const [productName, setProductName] = useState("")
	const [productDescription, setProductDescription] = useState("")
	const [productImage, setProductImage] = useState("")
	const [stocks, setStocks] = useState("")
	const [weight, setWeight] = useState("")
	const [price, setPrice] = useState("")

	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(() => {

		if(productName !== "" && productDescription !== "" && (stocks !== "" && stocks > 0) && (weight != "" && weight > 0) && (price !== "" && price > 0)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[productName, productDescription, stocks, weight, price])


	function addProduct(e) {
		e.preventDefault()

		fetch('https://quiet-badlands-82282.herokuapp.com/api/products', {

			method: "POST",
			headers: {
				"Content-type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				productDescription: productDescription,
				productImage: productImage,
				stocks: stocks,
				weight: weight,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message) {
				Swal.fire({
					icon: "error",
					title: data.message,
					text: "Please enter a valid number"
				})
			} else {

				Swal.fire({
					icon: "success",
					title: "Product Added Successfully!",
					text: "Product has been created"
				})

				setWillRedirect(true)
			}

		})

		setProductName("")
		setProductDescription("")
		setProductImage("")
		setStocks("")
		setWeight("")
		setPrice("")

	}

	return (


		!user.isAdmin || willRedirect

		?
			<Redirect to="/products" />
		:

		<div className="create-product-section">
			<div className="row justify-content-center">
				<div id="create-product-box" className="col-lg-5">
					<div className="sign-header">
						<h2>ADD PRODUCT</h2>
					</div>
					<Form onSubmit={e=> addProduct(e)} className="form-box">
						<Form.Group className="form-item">
							<Form.Label>NAME</Form.Label>
							<Form.Control 
								type="text" 
								value={productName} 
								onChange={e=>{setProductName(e.target.value)}} 
								required/>
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>DESCRIPTION</Form.Label>
							<Form.Control 
								as="textarea" 
								value={productDescription} 
								onChange={e=>{setProductDescription(e.target.value)}} 
								required/>
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>IMAGE URL</Form.Label>
							<Form.Control 
								type="text" 
								value={productImage} 
								onChange={e=>{setProductImage(e.target.value)}} 
								required/>
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>STOCKS</Form.Label>
							<InputGroup>
								<Form.Control 
									type="number" 
									value={stocks} 
									onChange={e=>{setStocks(e.target.value)}} />
								<InputGroup.Text>pcs</InputGroup.Text>
							</InputGroup>
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>WEIGHT</Form.Label>
							<InputGroup>
								<Form.Control 
									type="number" 
									value={weight} 
									onChange={e=>{setWeight(e.target.value)}} />
								<InputGroup.Text>grams</InputGroup.Text>
							</InputGroup>
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>PRICE</Form.Label>
							<InputGroup>
								<InputGroup.Text>Php</InputGroup.Text>
								<Form.Control 
									type="number" 
									value={price} 
									onChange={e=>{setPrice(e.target.value)}} />
							</InputGroup>
						</Form.Group>

						<div className="sign-button">
							<Button variant="custom" type="submit" disabled={isActive==false}>Add</Button>
						</div>
					</Form>
				</div>
			</div>
		</div>



	)
}
