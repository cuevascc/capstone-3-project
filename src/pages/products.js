import React, {useState, useEffect, useContext} from 'react'
import {Table, Button} from 'react-bootstrap'
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'

import Product from '../components/Product'
import UserContext from '../userContext'

export default function Products () {

	const {user} = useContext(UserContext)

	const [allProducts, setAllProducts] = useState([])
	const [activeProducts, setActiveProducts] = useState([])
	const [update, setUpdate] = useState(0)
	const [willRedirect, setWillRedirect] = useState(false)
	const [storedData, setStoredData] = useState({})


	useEffect(()=> {

		fetch('https://quiet-badlands-82282.herokuapp.com/api/products')
		.then(res => res.json())
		.then(data => {

			if (!data.products)
				return

			setAllProducts(data.products)

			let productsTemp = data.products

			let tempArray = productsTemp.filter(product => {
				return product.isActive === true
			})

			setActiveProducts(tempArray)

		})



	}, [update])


	let productComponents =  activeProducts && activeProducts.map((product) => {
		return (

			<Product key={product._id} productProp={product} />

			)
	});


	function archive (productId) {
		fetch(`https://quiet-badlands-82282.herokuapp.com/api/products/archive/${productId}`, {

			method: 'PUT',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUpdate({})
			
		})
	}

	function activate (productId) {

		fetch(`https://quiet-badlands-82282.herokuapp.com/api/products/activate/${productId}`, {

			method: 'PUT',
			headers: {
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setUpdate({})

		})
		
	}



	function remove (productId) {

		Swal.fire({
			title: "Delete product?",
			showCancelButton: true,
			confirmButtonText: "Yes",
			confirmButtonColor: '#eb967a',
			allowOutsideClick: false

		}).then((res)=> {
			if(res.isConfirmed) {
				fetch(`https://quiet-badlands-82282.herokuapp.com/api/products/remove/${productId}`, {
					method: 'DELETE',
					headers: {

							'Authorization':`Bearer ${localStorage.getItem('token')}`
					}
				})
				.then(res=> res.json())
				.then(data => {
					Swal.fire({
								icon: "success",
								title: "Product Deleted Successfuly!",
								confirmButtonColor: '#eb967a'
							})
					setUpdate({})
				})
			} 
		})

	}


	function editDetails (productId) {

		fetch(`https://quiet-badlands-82282.herokuapp.com/api/products/${productId}`)

		.then(res => res.json())
		.then(data => {

			data = data.searchResult

			setStoredData(data)

			setWillRedirect(true)

			
		})

	}

	let productRows = allProducts && allProducts.map(product => {
		return (


				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.productName}</td>
					<td>{product.price}</td>
					<td>{product.stocks}</td>
					<td>{product.weight}</td>
					<td>{product.createdAt}</td>
					<td>{product.updatedAt}</td>
					<td className={product.isActive ? "text-success" : "text-muted"}>{product.isActive ? "Active" : "Inactive"}</td>
					<td>
						{
							product.isActive 

							?								
									<Button className="archive-btn" onClick={()=> archive(product._id)}>Archive</Button>
							
							:
									<Button className="activate-btn" onClick={()=> activate(product._id)}>Activate</Button>
								

							
						}

						
							<Button className="edit-btn" onClick={()=> editDetails(product._id)}>Edit</Button>
							<Button className="delete-btn" onClick={()=> remove(product._id)}>Delete</Button>

						
						
					</td>
				</tr>


		)
	})


	return (

			user.isAdmin 

			?

			<>

				{willRedirect && (<Redirect to={{
					pathname: "/editproductdetails",
					state: storedData
				}}/>)}

			
				<div className="table-container">
					{
						allProducts.length === 0

						?

						<div className="cart-msg">
							No existing product as of the moment.
						</div>

						:		
								
						<Table className="products-list" striped bordered hover>
							<thead>
								
								<tr>
									<th>Product ID</th>
									<th>Name</th>
									<th>Price</th>
									<th>Stocks</th>
									<th>Weight (g)</th>
									<th>Created</th>
									<th>Updated</th>							
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
								<tbody>
									{productRows}
								</tbody>
							
						</Table>
					}
				</div>
				
			</>

			:

			<>
				<div>
					<div className="row">
						<div className="col-lg-10 page-heading">
							PRODUCTS
						</div>
					</div>
				</div>
					{productComponents}
			</>


		)
}