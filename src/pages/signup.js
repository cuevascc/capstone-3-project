import React, {useState,useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'

import {Redirect} from 'react-router-dom'

export default function SignUp () {

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)
  
	useEffect(()=> {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)) {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password, confirmPassword]);

	function registerUser(e) {
		e.preventDefault()
		
		fetch('https://quiet-badlands-82282.herokuapp.com/api/register', {

			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password

			})
		})
		.then(response => response.json())
		.then(data => {
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Registration Failed",
					confirmButtonColor: '#eb967a',
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					confirmButtonColor: '#eb967a'
				})

				setWillRedirect(true)
			}
		})

		setFirstName("")
		setLastName("")
		setEmail("")
		setMobileNo("")
		setPassword("")
		setConfirmPassword("")

	}

	return (

			willRedirect

			?
				<Redirect to="/"/>
			:
				<div>
					<div className="row justify-content-center">
						<div id="register-box" className="col-lg-5">
							<div className="sign-header">
								<h2>SIGN UP</h2>
							</div>
							<Form className="form-box" onSubmit={e=>registerUser(e)}>
								<Form.Group className="form-item">
									<Form.Label className="form-label">FIRST NAME</Form.Label>
									<Form.Control 
										type="text" 
										value={firstName} 
										onChange={e=>{setFirstName(e.target.value)}} 
										required/>
								</Form.Group>

								<Form.Group className="form-item">
									<Form.Label className="form-label">LAST NAME</Form.Label>
									<Form.Control 
										type="text" 
										value={lastName} 
										onChange={e=>{setLastName(e.target.value)}} 
										required/>
								</Form.Group>

								<Form.Group className="form-item">
									<Form.Label className="form-label">EMAIL</Form.Label>
									<Form.Control 
										type="email" 
										value={email} 
										onChange={e=>{setEmail(e.target.value)}} 
										required/>
								</Form.Group>

								<Form.Group className="form-item">
									<Form.Label className="form-label">MOBILE NUMBER</Form.Label>
									<Form.Control 
										type="text" 
										value={mobileNo} 
										onChange={e=>{setMobileNo(e.target.value)}} 
										required/>
								</Form.Group>

								<Form.Group className="form-item">
									<Form.Label className="form-label">PASSWORD</Form.Label>
									<Form.Control 
										type="password" 
										value={password} 
										onChange={e=>{setPassword(e.target.value)}} 
										required/>
								</Form.Group>

								<Form.Group className="form-item">
									<Form.Label className="form-label">CONFIRM PASSWORD</Form.Label>
									<Form.Control 
										type="password" 
										value={confirmPassword} 
										onChange={e=>{setConfirmPassword(e.target.value)}} 
										required/>
								</Form.Group>

									<div class="sign-button">
									  <Button variant="custom" type="submit" disabled={isActive==false}>
									    Sign Up
									  </Button>
									</div>
							</Form>
							<div className="signin-to-reg">
									Already a member? <a href="/signin">Sign In Here</a> 
							</div>
						</div>
					</div>
				</div>
		)
}