import React, {useEffect, useState} from 'react'
import {Table, Card} from 'react-bootstrap'

export default function UserProfile () {

	const [userOrders, setUserOrders] = useState([])
 
	useEffect(()=> {
		fetch('https://quiet-badlands-82282.herokuapp.com/api/user/orders', {
				method: 'GET',
				headers: {
					'Authorization':`Bearer ${localStorage.getItem('token')}`
				}
		})
		.then(res => res.json())
		.then(data => {
			if(!data.orders)
				return
			setUserOrders(data.orders)
		})
	}, [])

	let userOrdersList = userOrders && userOrders.map(userOrder => {
		return (
			<tr key={userOrder._id}>
				<div>
				{
					userOrder.status
					?
						<div className="order-fulfilled">Order Complete</div>

					: 
						<div className="order-pending">To Receive</div>

				}
					<div className="row">
						<div className="col">
							Reference #: {userOrder._id}
						</div>	
					</div>
					<div className="row">
						<div className="col">
							Date of Purchase: {userOrder.purchasedOn}
						</div>	
					</div>
					<div className="row">
						<div className="col">
							Shipping Address: {userOrder.shippingAddress}, {userOrder.region}
						</div>	
					</div>
					<div className="row">
						<div className="col">
							Subtotal: ₱ {userOrder.subTotal && userOrder.subTotal.toFixed(2)} 
							 
						</div>	
					</div>
					<div className="row">
						<div className="col">
							ShippingFee: ₱ {userOrder.shippingFee && userOrder.shippingFee.toFixed(2)}
						</div>	
					</div>
					<div className="row">
						<div className="col">
							Total Payment: ₱ {userOrder.total && userOrder.total.toFixed(2)}
						</div>	
					</div>								
				</div>				
			</tr>
		)
	})

	return (
		<div>
			<div className="row justify-content-center">
				<div className="col-lg-7">
					<Card className="cart">
					<div>
						<div className="row justify-content-center">
							<div className="col-lg-10 purchases-heading">
								PURCHASES
							</div>
						</div>
					</div>
						<Table className="my-orders-list" striped bordered hover>
							{userOrders.length === 0

							?
								<div className="cart-msg">
									You have no orders.
								</div>
							:
							<tbody>
								{userOrdersList}								
							</tbody>
							}
						</Table>
					</Card>
				</div>
			</div>
		</div>
		
	)
}