import React, {useState, useContext} from 'react'
import {Redirect} from 'react-router-dom'
import {Card, Button, Table} from 'react-bootstrap'

import UserContext from '../userContext'

export default function Cart ({cart, emptyCart}) {
	let subTotal = 0

	const {user} = useContext(UserContext)

	const [render, reRender] = useState(0)
	const [willRedirect, setWillRedirect] = useState(false)

	cart.forEach(item => {
		let eachTotal = (item.price*item.quantity)
		subTotal += eachTotal
	})

	let cartList = cart.map(cartItem => {
		return (
			<tr key={cartItem.productId}>
				<div>
					<div className="row">
						<div className="col">
							<h5>{cartItem.productName}</h5>
						</div>
						<div className="col remove-item">
							<Button className="remove-item-btn" onClick={() => deleteItem(cartItem.productId)}>×</Button>
						</div>	
					</div>
					<div className="row">
						<div className="col">
							Price: ₱ {cartItem.price.toFixed(2)}
						</div>	
					</div>
					<div className="row">
						<div className="col">
							Quantity: {cartItem.quantity} pcs. 
						</div>	
					</div>				
				</div>		
			</tr>
		)
	})

	function deleteItem (key) {
		let index = cart.findIndex(item => item.productId == key)
		cart.splice(index, 1)
		reRender({})
	}

	function checkOut () {
		setWillRedirect(true)
	}

	
	return (
		<>
			{willRedirect && (<Redirect to={{
				pathname: "/checkout",
				state: cart,
				emptyCart: emptyCart
			}}/>)}

			<div>
				<div className="row justify-content-center">
					<div className="col-lg-5">
						<Card className="cart">
						<div>
							<div className="row justify-content-center">
								<div className="col-lg-10 cart-page-heading">
									C A R T
								</div>
							</div>
						</div>
							<Table className="cart-rows" striped hover>
								{cart.length == 0

								?
									<div className="cart-msg">
										Your cart is empty.
									</div>
								:
								<tbody>
									{cartList}								
								</tbody>
								}
							</Table>
							<Card.Text>Subtotal: ₱ {subTotal.toFixed(2)}</Card.Text>
							<Button 
							 	className="check-out-btn" 
								disabled={cart.length==0}
								onClick={checkOut}>Check Out
							</Button> 
						</Card>
					</div>
				</div>
			</div>
		</>

	)
}
