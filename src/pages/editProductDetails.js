import React, {useState, useEffect, useContext} from 'react'
import {useLocation, Redirect} from "react-router-dom";
import {Form, Button, InputGroup} from 'react-bootstrap'
import Swal from 'sweetalert2'

import UserContext from '../userContext'

export default function EditProductDetails () {

	const {user} = useContext(UserContext)

	let location = useLocation();

	const [productName, setProductName] = useState(location.state.productName)
	const [productDescription, setProductDescription] = useState(location.state.productDescription)
	const [productImage, setProductImage] = useState(location.state.productImage)
	const [stocks, setStocks] = useState(location.state.stocks)
	const [weight, setWeight] = useState(location.state.weight)
	const [price, setPrice] = useState(location.state.price)
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(() => {
	
		if(stocks > 0 && weight > 0 && price > 0 && productName !== "" && productDescription !== "" && productImage !== "")
		{
			setIsActive(true)
		
		} else {

			setIsActive(false)
		}

	},[productName, productDescription, productImage, weight, stocks, price])

	function editProduct(e) {
		e.preventDefault()

		fetch(`https://quiet-badlands-82282.herokuapp.com/api/products/update/${location.state._id}`, {

			method: "PUT",
			headers: {
				"Content-type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				productName: productName,
				productDescription: productDescription,
				productImage: productImage,
				weight: weight,
				stocks: stocks,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.message){
				Swal.fire({
					icon: "error",
					title: data.message,
					confirmButtonColor: '#eb967a',
					text: "Please enter a valid number"
				})
			} else {
				Swal.fire({
					icon: "success",
					confirmButtonColor: '#eb967a',
					title: "Product Updated Successfully!"
					
				})

				setWillRedirect(true)
			}
		
		})

		setProductName("")
		setProductDescription("")
		setProductImage("")
		setStocks("")
		setPrice("")
	}

	return (

		user.isAdmin === false || willRedirect

		? 
			<Redirect to="/products" />
		:

		<div className="create-product-section">
			<div className="row justify-content-center">
				<div id="create-product-box" className="col-lg-5">
					<div className="sign-header">
						<h2>EDIT PRODUCT DETAILS</h2>
					</div>
					<Form onSubmit={e=> editProduct(e)} className="edit-product-form">
						<Form.Group className="form-item">
							<Form.Label>NAME:</Form.Label>
							<Form.Control 
								type="text" 
								value={productName} 
								onChange={e=>{setProductName(e.target.value)}} />
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>DESCRIPTION:</Form.Label>
							<Form.Control 
								as="textarea" 
								value={productDescription} 
								onChange={e=>{setProductDescription(e.target.value)}} />
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>IMAGE URL:</Form.Label>
							<Form.Control 
								type="text" 
								value={productImage} 
								onChange={e=>{setProductImage(e.target.value)}} />
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>STOCKS:</Form.Label>
							<InputGroup>
								<Form.Control 
									type="number" 
									value={stocks} 
									onChange={e=>{setStocks(e.target.value)}} />
								<InputGroup.Text>pcs</InputGroup.Text>
							</InputGroup>
						</Form.Group>

						<Form.Group className="form-item">
							<Form.Label>WEIGHT:</Form.Label>
							<InputGroup>
								<Form.Control 
									type="number" 
									value={weight} 
									onChange={e=>{setWeight(e.target.value)}} />
								<InputGroup.Text>grams</InputGroup.Text>
							</InputGroup>
						</Form.Group>
						
						<Form.Group className="form-item">
							<Form.Label>PRICE:</Form.Label>
							<InputGroup>
								<InputGroup.Text>Php</InputGroup.Text>
								<Form.Control 
									type="number" 
									value={price} 
									onChange={e=>{setPrice(e.target.value)}} />
							</InputGroup>
						</Form.Group>

						<div className="sign-button">
							<Button 
								variant="custom" 
								disabled={isActive == false} 
								type="submit">
								    Save
							</Button>
						</div>
					</Form>
				</div>
			</div>
		</div>
	
	)
}